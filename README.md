#oschat

    oschat基于HTML5 WebSocket的开源IM，后台服务基于spring+ undertow + akka. 客户端使用了node-webkit，使用html5+js进行开发. 客户端完全用了web相关技术，所有很方便的进行二次开发和集成.
    
    当前版本 0.0.1-SNAPSHOT
    

##说明

* [说明文章](http://my.oschina.net/FengJ/blog/157416)


##目前在使用java重写后台服务...

##客户端

* 客户端使用了 [node-webkit](https://github.com/rogerwang/node-webkit) 进行开发，支持跨平台。
* client/code 文件中，放了node-webkit的windows客户端，可直接运行nw.exe打开。
* 更多node-webkit说明，访问 https://github.com/rogerwang/node-webkit