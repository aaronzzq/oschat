/**
 * Created by clarie on 14-7-24.
 */
(function(){
    oschat.LoginSet=LoginSet;
    var EXTEND=null;
    function LoginSet(){
        if(EXTEND){
            EXTEND.apply(this.arguments);
        }
        this._ok=$('#ok');
        this._cancel=$('#cancel');
        this._ok.click(function(){
            $('#loginSet').hide();
            $('body')[0].style.background='url(imgs/login.png) no-repeat'
            $('#loginPage').show();

        });
        this._cancel.click(function(){
            $('#loginSet').hide();
            $('body')[0].style.background='url(imgs/login.png) no-repeat'
            $('#loginPage').show();
        });
    }
})();