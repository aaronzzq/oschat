package org.topteam.oschat.event;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.topteam.oschat.entity.User;

/**
 * 用户自主行为相关事件
 * 
 * @author JiangFeng
 * 
 */
public class UserEvent {

	/**
	 * 用户登录事件
	 * 
	 * @author JiangFeng
	 * 
	 */
	public static class Login extends Event implements Serializable {

		private static final long serialVersionUID = -7430185558396236914L;
		private User user;

		public Login(User user) {
			super();
			super.setType("Login");
			this.user = user;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}

	}

	/**
	 * 推送所有好友数据
	 * 
	 * @author JiangFeng
	 * 
	 */
	public static class Firends extends Event implements Serializable {

		private static final long serialVersionUID = 6082670347668980110L;
		private Map<String, List<User>> firends = new HashMap<String, List<User>>();

		public Firends() {
			super();
			super.setType("Firends");
		}

		public Map<String, List<User>> getFirends() {
			return firends;
		}

		public void setFirends(Map<String, List<User>> firends) {
			this.firends = firends;
		}

	}

	/**
	 * 查询所有好友数据
	 * 
	 * @author JiangFeng
	 * 
	 */
	public static class GetFirends extends Event implements Serializable {

		private static final long serialVersionUID = 675281661164367655L;

		public GetFirends() {
			super();
			super.setType("GetFirends");
		}

	}

	/**
	 * 用户推出事件
	 * 
	 * @author JiangFeng
	 * 
	 */
	public static class Logout extends Event implements Serializable {

		private static final long serialVersionUID = -4855552674859219156L;
		private User user;

		public Logout(User user) {
			super();
			super.setType("Logout");
			this.user = user;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}
	}

}
