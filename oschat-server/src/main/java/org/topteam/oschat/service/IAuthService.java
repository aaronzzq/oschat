package org.topteam.oschat.service;

public interface IAuthService {

	void addToken(String token, String userId);

	String getToken(String token);
	
}
