package org.topteam.oschat;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.topteam.oschat.dao.KvDao;
import org.topteam.oschat.entity.Band;
import org.topteam.oschat.entity.User;
import org.topteam.oschat.entity.UserRelation;

@Component
public class DataIniter {

	@Autowired
	private KvDao dao;

	public void init() {
		Map<String, User> userMap = new HashMap<String, User>();
		
		dao.open(UserRelation.TAG);
		for (int i = 1; i <= 10; i++) {
			User user = new User();
			user.setUserId("aac"+String.valueOf(i));
			user.setPassword(String.valueOf(i));
			user.setPortrait("xxx");
			user.setResume("Hi, oschat !");
			user.setUsername("User" + i);
			userMap.put(user.getUserId(), user);
			UserRelation relation = new UserRelation();
			relation.setUserId(user.getUserId());
			Band band = new Band();
			band.setName("我的好友");
			band.getUsers().add(user.getUserId());
			relation.getBands().add(band);
			dao.putObj(user.getUserId(), relation);
		}
		dao.open(User.TAG).putObj(userMap);
		System.out.println("user size:" + dao.open(User.TAG).stats());
		
		Map<String,String> map = dao.open(User.TAG).query("aac");
		System.out.println(map.size());
		
		System.out.println(dao.open(User.TAG).get("xxx"));
		
		dao.close();
	}
}
